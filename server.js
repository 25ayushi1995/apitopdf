require('rootpath')()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const connectDB = require('config')
const errorHandler = require('helpers/error-handlers')
const PORT = require('constants/serverConst').port
const PDFrouter = require('routes')
const app = express()

connectDB()
app.use(helmet())
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: false
  })
)
app.set('view engine', 'ejs')
app.use(cors())

app.use(express.static('static'))
app.use('/pdf', PDFrouter)
app.use(errorHandler)
app.listen(PORT, () => console.log('connected to server'))
