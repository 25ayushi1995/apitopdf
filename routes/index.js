const express = require('express')
const router = express.Router()
const PDFController = require('../controllers')

router.get('/', PDFController.dynamicHTMLConverter)

module.exports = router
