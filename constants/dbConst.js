const envCheck = require('./')
let dbUrl
if (envCheck()) {
  dbUrl = ''
} else {
  dbUrl = 'mongodb://localhost:27017/apitoPdf'
}

exports.dbUrl = dbUrl
