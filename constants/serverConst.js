const envCheck = require('./')

let port
if (envCheck()) {
  port = process.env.PORT
} else {
  port = 4000
}

exports.port = port
