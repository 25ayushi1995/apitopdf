const mongoose = require('mongoose')

const PDFDetails = new mongoose.Schema({
  responseId: {
    type: String
  },
  pdfUrl: {
    type: String
  },
  s3BucketUrl: {
    type: String
  }
})

let PDF = mongoose.model('pdf', PDFDetails)

module.exports = PDF
