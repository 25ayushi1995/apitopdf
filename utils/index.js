const Api2Pdf = require('api2pdf')
const a2pClient = new Api2Pdf('23a7b8c1-3ec1-438f-a77d-2be4f0209309')
const PDF = require('../models')

const HTMLTOPDF = html => {
  return a2pClient.wkhtmltopdfFromHtml(html).then(
    async function (result) {
      let params = {
        responseId: result.responseId,
        pdfUrl: result.pdf,
        s3BucketUrl: ''
      }
      const PdfDocument = new PDF(params)
      let pdf = await PdfDocument.save()
      return {
        result: pdf,
        success: result.success
      }
    },
    function (err) {
      return {
        result: err,
        success: result.success
      }
    }
  )
}

module.exports = {
  HTMLTOPDF
}
