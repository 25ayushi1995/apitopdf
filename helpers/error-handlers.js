module.exports = function (req, res, data, callback) {
  var responseData = {
    success: function (message, code) {
      console.log(message, code)
      res.status(code).json({
        is_success: true,
        message: message,
        responseCode: code
      })
    },
    failure: function (error, code, message) {
      res.status(code).json({
        is_success: false,
        message: error,
        responseCode: code
      })
    },
    data: function (item, code) {
      res.status(code).json({
        is_success: true,
        data: item,
        responseCode: code
      })
    },
    page: function (items, total, page_no, code) {
      res.status(code).json({
        is_success: true,
        data: {
          items: items,
          skip: page_no || 0,
          total: total || items.length
        },
        responseCode: code
      })
    }
  }
  return callback(responseData)
}
