const htmltoPDf = require('../utils').HTMLTOPDF
const fs = require('fs')
const AWS = require('aws-sdk')
const request = require('request')
const randomstring = require('randomstring')
const s3 = new AWS.S3({
  accessKeyId: 'AKIA2XL6Z2SVZNEIFTG7',
  secretAccessKey: '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'
})
const PDF = require('../models')
const PDFController = {}

PDFController.dynamicHTMLConverter = (req, res) => {
  res.render('home', async (err, html) => {
    if (err) {
      failure(err, 400)
    }
    let htm2 = await htmltoPDf(html)
    if (htm2.success) {
      request.head(htm2.result.pdfUrl, function (err, res, body) {
        console.log('content-type:', res.headers['content-type'])
        console.log('content-length:', res.headers['content-length'])
        let filename = randomstring.generate({
          length: 5,
          charset: 'alphabetic'
        })

        request(htm2.result.pdfUrl).pipe(
          fs.createWriteStream(`uploads/${filename}.pdf`)
        )
        const fileContent = fs.readFileSync(`uploads/${filename}.pdf`)
        const params = {
          Bucket: 'pa-project',
          Key: `${filename}.pdf`, // File name you want to save as in S3
          Body: fileContent
        }
        s3.upload(params, function (err, data) {
          if (err) {
            throw err
          }
          console.log(`File uploaded successfully. ${data.Location}`)
          PDF.updateOne(
            { _id: htm2.result._id },
            { $set: { s3BucketUrl: data.Location } },
            result => console.log(result)
          )
        })

        //   .on('close', callback)
      })

      res.status(200).json({
        status: 200,
        result: htm2.result
      })
    } else {
      res.status(400).json({
        status: 400,
        result: htm2.result
      })
    }
  })
}

module.exports = PDFController
